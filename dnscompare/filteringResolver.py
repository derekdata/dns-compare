import dns.resolver
import sys
import logging
from pprint import pprint

logging.basicConfig(level=logging.DEBUG, stream=sys.stdout)

class FilteringResolver:
    def __init__(self, name, resolverIps, failIps):
        self.resolver = dns.resolver.Resolver(configure=False)
        self.resolver.nameservers = resolverIps
        self.name = name
        self.failIps = failIps       
        self.passCount = 0

    def resolve(self, index, domain, resultsDict):
        for attempt in range(10):
            try:
                result = self.resolver.query(domain, lifetime=5)
                #pprint(vars(result))
                failReason='No IPs'
                for ipval in result:
                    #If IP is a known failure IP, mark as a failure
                    if str(ipval) in self.failIps:
                        failReason=' '.join(['Returned Failure IP', str(ipval)])
                        pass
                    else:
                        logging.info(' '.join([domain, 'pass', self.name, 'IP', str(ipval)]))
                        resultsDict[self.name] = True
                        self.passCount+=1
                        return
            except dns.resolver.NXDOMAIN:
                failReason='NXDOMAIN'
                pass 
            except:
                failReason=str(sys.exc_info()[0])
                #logging.exception("message")
                logging.debug(' '.join([domain, 'retrying attempt', str(attempt), str(self.name), str(sys.exc_info()[0])]))
                continue
            break
        logging.info(' '.join([domain, 'fail', self.name, failReason]))
        resultsDict[self.name] = False
        return