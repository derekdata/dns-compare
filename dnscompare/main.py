import dns.resolver
import getopt
import logging
import os
import sys
import time
import threading

from filteringResolver import FilteringResolver


def write_output_header(outputFile, resolvers):
    header = 'Domain,'
    for filteringResolver in resolvers:
        header += filteringResolver.name + ','
    outputFile.write(header[0:len(header)-1]+'\n')


def write_output_results(outputFile, resolvers, domain, resultsDict):
    results = domain + ','
    for filteringResolver in resolvers:
        if (resultsDict[filteringResolver.name]):
            results += '1,'
        else:
            results += '0,'
    outputFile.write(results[0:len(results)-1]+'\n')


def main(argv):

    logging.basicConfig(level=logging.DEBUG, stream=sys.stdout)

    # Parse arguments
    inputFileName = os.getcwd() + "/hosts-sample"
    outputFileName = "/tmp/dns-check-results"
    try:
        opts = getopt.getopt(argv, "hi:o:")
    except getopt.GetoptError:
        print('main.py -i <inputfile> -o <outputfile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('main.py -i <inputfile> -o <outputfile>')
            sys.exit()
        elif opt in ("-i"):
            inputFileName = arg
        elif opt in ("-o"):
            outputFileName = arg
    print('Input file:', inputFileName)
    print('Output file:', outputFileName)

    # Intialize resolvers
    resolvers = [
        FilteringResolver('AdGuard Family', ['176.103.130.132', '176.103.130.134'], [
                          '176.103.130.135']),
        FilteringResolver('CleanBrowsing Adult', [
                          '185.228.168.10', '185.228.169.11'], []),
        FilteringResolver('CleanBrowsing Family', [
                          '185.228.168.168', '185.228.169.168'], []),
        FilteringResolver('CleanBrowsing Security', [
                          '185.228.168.9', '185.228.169.9'], []),
        FilteringResolver('CloudFlare Family', [
                          '1.1.1.3', '1.0.0.3'], ['0.0.0.0']),
        FilteringResolver('CloudFlare Regular', ['1.1.1.1', '1.0.0.2'], []),
        FilteringResolver('DNS For Family', [
                          '94.130.180.225', '78.47.64.161'], ['159.69.10.249']),
        FilteringResolver('Neustar FamilySecure', ['156.154.70.3', '156.154.71.3'], [
                          '156.154.112.17', '156.154.113.17']),
        FilteringResolver('OpenDNS FamilyShield', [
                          '208.67.222.123', '208.67.220.123'], ['146.112.61.106']),
        #FilteringResolver('Yandex Family',['77.88.8.7','77.88.8.3'],['93.158.134.250']),
    ]

    # Initialize variables
    domainCount = 0
    threads = list()
    inputFile = open(inputFileName, "r")
    outputFile = open(outputFileName, "w")

    write_output_header(outputFile, resolvers)

    for line in inputFile:
        domain = line.split()[0].rstrip()
        resultsDict = dict()

        for index, filteringResolver in enumerate(resolvers):
            x = threading.Thread(target=filteringResolver.resolve, args=(
                index, domain, resultsDict))
            threads.append(x)
            x.start()

        for thread in threads:
            thread.join()

        domainCount += 1

        write_output_results(outputFile, resolvers, domain, resultsDict)

    for filteringResolver in resolvers:
        percentBlocked = round(
            (((domainCount-filteringResolver.passCount)/domainCount)*100), 1)
        print(filteringResolver.name, ": ", domainCount -
              filteringResolver.passCount, " (", percentBlocked, "%)", sep='')

    print('Domains analyzed', domainCount)
    inputFile.close()
    outputFile.close()


if __name__ == "__main__":
    main(sys.argv[1:])
