# DNS Compare

DNS Compare compares the blocking capabilities of DNS servers.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

* Python 3.x
* [dnspython](https://pypi.org/project/dnspython/)

### Installing

Install the prerequisites listed above

## Running the tests

Mock/Hermetic Unit Tests 
```
python3 -m unittest test.test_filteringResolver -v
```

All Tests, including integration tests that rely on remote servers
```
python3 -m unittest discover -v
```

## Versioning

We use [SemVer](http://semver.org/) for versioning. 

## License

This project is licensed under the GPLv3 License - see the [LICENSE](LICENSE) file for details