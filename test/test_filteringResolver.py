import unittest
from unittest.mock import patch

import dns.resolver
from dnscompare.filteringResolver import FilteringResolver

class TestFilteringResolver(unittest.TestCase):

    @patch('dns.resolver.Resolver.query')
    def test_successful_lookup(self, mock_dns_resolver_query):
        mock_dns_resolver_query.return_value = ['4.4.4.4']
        resultsDict = dict()
        resolver = FilteringResolver('CloudFlare Regular',['1.1.1.1','1.0.0.2'],[])
        resolver.resolve(10,"gitlab.com",resultsDict)
        self.assertTrue(resultsDict[resolver.name])

    @patch('dns.resolver.Resolver.query')
    def test_failed_lookup_nxdomain(self, mock_dns_resolver_query):
        mock_dns_resolver_query.side_effect = dns.resolver.NXDOMAIN
        resultsDict = dict()
        resolver = FilteringResolver('CloudFlare Regular',['1.1.1.1','1.0.0.2'],[])
        resolver.resolve(10,"gitlab.com",resultsDict)
        self.assertFalse(resultsDict[resolver.name])
    
    @patch('dns.resolver.Resolver.query')
    def test_failed_lookup_noanswer(self, mock_dns_resolver_query):
        mock_dns_resolver_query.side_effect = dns.resolver.NoAnswer()
        resultsDict = dict()
        resolver = FilteringResolver('CloudFlare Regular',['1.1.1.1','1.0.0.2'],[])
        resolver.resolve(10,"gitlab.com",resultsDict)
        self.assertFalse(resultsDict[resolver.name])

if __name__ == '__main__':
    unittest.main()