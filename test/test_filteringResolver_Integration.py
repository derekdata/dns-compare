import unittest
from dnscompare.filteringResolver import FilteringResolver


class TestFilteringResolverIntegration(unittest.TestCase):

    def test_cloudflare_integration(self):
        resultsDict = dict()
        resolver = FilteringResolver('CloudFlare Regular', [
                                     '1.1.1.1', '1.0.0.2'], [])
        resolver.resolve(10, "gitlab.com", resultsDict)
        self.assertTrue(resultsDict[resolver.name])

    def test_opendns_integration(self):
        resultsDict = dict()
        resolver = FilteringResolver('OpenDNS FamilyShield', [
                                     '208.67.222.123', '208.67.220.123'], ['146.112.61.106'])
        resolver.resolve(10, "gitlab.com", resultsDict)
        self.assertTrue(resultsDict[resolver.name])

        resultsDict = dict()
        resolver.resolve(10, "exampleadultsite.com", resultsDict)
        self.assertFalse(resultsDict[resolver.name])


if __name__ == '__main__':
    unittest.main()
